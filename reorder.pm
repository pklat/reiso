#!/usr/bin/perl
# reorder v0.92

use 5.012;
use strict;
use warnings;

use utf8;
use File::Find::Rule;
use List::Util qw(first);
use IO::Handle;
use Data::Dumper;

if (not $ARGV[1] ) {print "usage:\n reorder.pm inputdir/ outlist.txt\n"; exit;}

my ( $level, $packed, $dir_in, $size_raw, $size_packed, $delta, $delta_max, $delta_max2, $best_match, $best_match2, $total, $totaldmax, %datatype, %size );
my ( $file_out, $FILE0, $FILE1, $window, $half, $cmd, $sub );
my %done; # processed files

my %diff = ( lz4 => \&diff_lz4,
             zstd => \&diff_zstd,
             sevenzip => \&diff_7z 
           );
$sub = 'lz4';
$level = 2;
$packed = '/mnt/ramdisk/packed_qq.7z'; # RAMDISK
$total = $totaldmax = 0;
$dir_in = $ARGV[0];
$file_out = $ARGV[1];
$window = 1*1024*1024; # 1Mb
$half = $window/2;
# $window = 0; # uncomment to disable window
open( FILE0, ">", "$file_out" ) or die "Can't write file [$!]\n"; FILE0->autoflush(1);
open( FILE1, ">", "$file_out.stats" ) or die "Can't write file [$!]\n"; FILE1->autoflush(1);


my @symlinks = File::Find::Rule->symlink()->in("$dir_in");
my @files = File::Find::Rule->file()->not(File::Find::Rule->symlink())->in("$dir_in"); # ->maxdepth(20)->size('>41')->name(qr/j$|jg$/i)
if ( !$files[0] ) { print "no files found! \n"; exit; }
@files = sort @files;

# get datatypes
for (my $i = 0; $i <= $#files; $i++ ) {
# print "$files[$i]\n";  print `file -b $files[$i] | awk -F',' '{print \$1}'` . "\n";
$datatype{$i} = `file -b $files[$i] |awk -F',' '{print \$1}'`; chomp $datatype{$i};
$size{$i} = -s $files[$i];
}

unlink $packed;

for (my $i = 0; $total <= $#files-1; ) {
 print "$i: $files[$i]\n"; 
 $best_match = $best_match2 = $delta_max = $delta_max2 =undef;
 my $size_i = $size{$i};
 my $datatype_i = $datatype{$i};
 
 for (my $j = 0; $j <= $#files ; $j++) {
  if ( $j==$i or exists($done{$j}) ) { next; print FILE0 "skip i=$i j=$j \n"; next;} ; # preskace sebe ili nadjene
  next if ( length $best_match and $datatype_i ne $datatype{$j} ); # preskace drugi datatype
  
  $size_raw = $size_i + $size{$j}; # $options = $options . " -md=$size_raw" . 'b';
  &window ( $i, $j );
  $size_packed = $diff{$sub}->( $i, $j ); 
#   print "S=$size_packed, $files[$i] -> $files[$j]   $best_match $best_match2\n"; #exit;
  unlink $packed;
  $delta = $size_raw - $size_packed; #usteda
  if ( not length $delta_max or $delta > $delta_max ) {
   if ( length $best_match ) { $best_match2 = $best_match; $delta_max2 = $delta_max; };
   $delta_max = $delta;
   $best_match = $j;
  }
 }

#   print FILE0 " Dmax=$delta_max, $files[$i] -> $files[$best_match], $files[$best_match2]\n";
  print FILE0 "$files[$i]\n";
  print FILE1 "$i: $files[$i] -> $files[$best_match],  Dmax=$delta_max\n";
  $done{$i} = 1;
  $done{$best_match} = 1;
  $total = $total + 1;
  $totaldmax = $totaldmax + $delta_max;
  if ( length $best_match2 ) {
   $total = $total + 1;
   $totaldmax = $totaldmax + $delta_max2;
   $done{$best_match2} = 1;
   print FILE0 "$files[$best_match]\n";
   print FILE1 "$i: $files[$i] -> $files[$best_match2] Dmax2=$delta_max2\n";
   $i = $best_match2;
  }
  else { $i = $best_match; }
}


if ( length $best_match2) {
 print FILE0 "$files[$best_match2]\n";
 }
else {
 print FILE0 "$files[$best_match]\n";
}
 print FILE0 join("\n", @symlinks); 


print FILE1 "-\n symlinks:\n" . join("\n", @symlinks);
print FILE1 "--\n TOTAL D=$totaldmax\n";
 

close(FILE0); close(FILE1);

exit;











sub window
{
 my ( $i, $j ) = @_;
 my $trim=0;
 
 if ( $window == 0 ){ # disabled 
  $cmd = "cat $files[$i] $files[$j] |";
 }
 elsif ( $size_raw > $window and $size{$j} < $half ){ # drugi manji od $half  512
  $trim = $window-$size{$j};
  $cmd = "cat <( tail --bytes $trim $files[$i] )    <( cat $files[$j] ) |";
#   print FILE1 "size0:   $size{$i} + $size{$j}\n";
 }
 elsif ( $size_raw > $window and $size{$i} < $half ){ # prvi manji od 512
  $trim = $window-$size{$i};
  $cmd = "cat <( cat $files[$i] )    <( tail --bytes $trim $files[$j] )  |";
#   print FILE1 "size1:   $size{$i} + $size{$j}\n";
 }
 elsif ( $size_raw > $window and $size{$i} > $half and $size{$j} > $half ){ # oba veca od 512
  $cmd = "cat <( tail --bytes $half $files[$i] )    <( head --bytes $half $files[$j] )  |";
#   print FILE1 "size2:   $size{$i} + $size{$j}\n";
 }
 else { # ukupno manja od $window 1024
  $cmd = "cat $files[$i] $files[$j]  |";
#   print FILE1 "size0k:   $size{$i} + $size{$j}\n";
 }
 return;
}




sub diff_7z
{
 my ( $i, $j ) = @_;
 return `7z a -myx=9 -m0=LZMA -mx=$level -ms=on  $packed $files[$i] $files[$j]  | grep "Archive size:" | awk -F' ' '{print \$3}' `;
}


sub diff_zstd
{
 my ( $i, $j ) = @_;
 my $size;
 
 $cmd = $cmd . " zstd -$level -o /dev/null 2>&1 ";
 $size = `bash -c "$cmd"`;         # /*stdin*\            : 55.16%   (1048576 => 578410 bytes, /dev/null) 
 $size =~ m/ (\d+) bytes/;
 return $1;
}



sub diff_lz4
{
 my ( $i, $j ) = @_;
 my $size;
 
 $cmd = $cmd . " lz4 - $packed 2>&1 "; #print "$cmd\n"; exit;      Compressed 1048576 bytes into 725046 bytes ==> 69.15% 
 $size = `bash -c "$cmd"`;
 $size =~ m/into (\d+) bytes/;
 return $1;
}

