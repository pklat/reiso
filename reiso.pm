#!/usr/bin/perl
# reiso v1.7, by orange, usage:
#   perl reiso.pm dec in/mycd.iso out/ mount/
#   perl reiso.pm enc mycd.iso out/

use POSIX;
use strict;
use warnings;
use utf8;
use Encode qw(decode encode);
use File::Basename;
use Digest::SHA qw(sha256);
use Digest::CRC qw(crc32  crc32_hex);
use warnings FATAL => 'uninitialized';
use Data::Dumper; # print Dumper($boo);


my ( $fstart, $dir, $tmp_dir, $file, $iso, $iso_fn, $mode );
if ( @ARGV < 3 or @ARGV > 4 ) {
 print "Usage:\nto decode:\n reiso.pm   dec inputfile  output_dir/  mountpoint_dir/\nto recode:\n reiso.pm   enc outputfile  input_dir/\n";
 print "\n\nexample:\n perl reiso.pm dec in/mycd.iso out/ mount/\n";  print " perl reiso.pm enc mycd.iso out/\n"; exit;
}
$mode=$ARGV[0]; $iso=$ARGV[1]; $dir=$ARGV[2];
$tmp_dir=$ARGV[3] if ($ARGV[3] and $ARGV[0] eq 'dec'); # temporary mountpoints
$iso_fn = basename($iso);
if ( not -d $dir) { print "No such dir: $dir \nPlease create it first.\n"; exit; }
if ( $ARGV[0] eq 'dec' and not -d $tmp_dir ) { print "No temp dir (for mountp): $tmp_dir \nPlease create it first.\n";  exit; }


if ( $ARGV[0] eq 'dec' ) { print "Decoding $iso..\n";  &dec; }
elsif ( $ARGV[0] eq 'enc' ) {print "Encoding $iso..\n";  &enc; }

exit;


sub dec
{
  my $original_iso_size = -s "$iso";
  my $original_iso_sha256 = substr (`sha256sum "$iso"`, 0, 64);
  my ( @oall, $path);
  my $info0 = `isoinfo -d -i "$iso" 2>&1`;
  if ( $info0 =~ /Rock Ridge signatures version 1 found/i ) { print "err1: $info0\n"; } #exit; }
  my $info = `isoinfo -J -l -i "$iso" 2>&1`;
  if ( $info =~ /Unable to find Joliet SVD/i ) { print "noJO\n"; $info = lc `isoinfo -l -i "$iso"`; } #lc
  elsif ( $info =~ /NOT in ISO/i ) { print "err0: $info\n"; exit; }
  if ( $info0 =~ /Joliet with UCS level 1 found/i ) { print "noJO2\n"; $info = `isoinfo -l -i "$iso"`; }

 `mkdir -p "$dir/$iso_fn" $tmp_dir/isomnt/`; `fusermount -q -u $tmp_dir/isomnt/`; `fuseiso  "$iso" $tmp_dir/isomnt/`;
 `rsync -av $tmp_dir/isomnt/ "$dir/$iso_fn" `;  `chmod -R u+w "$dir/$iso_fn" ; sync `;

  push @oall, [-1, $original_iso_size, $original_iso_sha256, '*original_iso*', "$iso_fn" ];
  my @lines = split ( /\n/, $info );

  foreach my $line (@lines) {
    my ( $find, $crc32 );
    if ( $line=~/^Directory listing of (\/.*?)$/i ) { ( $path ) = ( $line=~/^Directory listing of \/(.+?)\/$/i ); }
    my ( $mask, $size, $offset, $type, $filename ) = ( $line=~/^.{14}\s+?(\d+)\s+?\d\s+?(\d+) .*?\D(\d+)\D(\d\d).  (.*)$/ );
    next unless ( $path and $filename and $type eq '00' );
    $path =  encode ( 'utf8', "$path");  $filename =  encode ( 'utf8', "$filename" );
    if ( $filename =~ / $/ ) { chop $filename; rename("$dir$iso_fn$path$filename ", "$dir$iso_fn$path$filename"); }
    if ( $filename =~ /;1$/ ) { chop $filename; chop $filename; rename("$dir$iso_fn$path$filename;1", "$dir$iso_fn$path$filename"); } # ne elsif
    if ( $filename =~ /\.$/ ) { chop $filename; rename("$dir$iso_fn$path$filename", "$dir$iso_fn$path$filename."); $filename.='.'; }
    my $dat =  `dd if='$iso' bs=2048 skip=$offset count=$size status=none iflag=count_bytes`;
    $crc32 = crc32_hex ( $dat );
    if ( !$crc32 ) { print "nocrc32: $offset,$size,$path$filename\n"; exit; }
    push @oall, [$offset, $size, $crc32, "$path", "$filename" ];
  }

  my @oall_sort = sort {$a->[0] <=> $b->[0] } @oall;
  my ($path_last, $file_last, $offset_last, $bsize_last);
  open( my $FILE1, ">", "$dir/$iso_fn.offs" ) or die "ERR: write file [$!]\n";


  for my $row (@oall_sort) {
    print $FILE1 join(",", @{$row}), "\n";
    next if ( @{$row}[0] == -1 );
    my( $offset, $size, $crc32, $path, $file ) = ( @{$row}[0], @{$row}[1], @{$row}[2], @{$row}[3], @{$row}[4] );
    my $bsize = ceil ($size/2048);
    if ( $size==0 and $crc32 eq '00000000' ) { next; } #nfo

    if ( ! length $offset_last  ) { # head
        print  " Extracting .iso header from 0 to $offset  count=$offset \n";
        `dd if="$iso"  bs=2048  count=$offset skip=0 conv=sync status=none > "$dir/$iso_fn.mdat"`;
    }
    elsif ( length $offset_last and $offset > $offset_last + $bsize_last ) {
     my $skip = $offset_last + $bsize_last;
     my $count = $offset - $skip;
     print " W: missing block, saving  to .mdat! offset=$offset  last: ($offset_last, $bsize_last) skip=$skip, count=$count\n"; # exit;
     `dd if="$iso" bs=2048 count=$count skip=$skip conv=sync status=none >> "$dir/$iso_fn.mdat"`;
    }
    elsif ( length $offset_last and $offset == $offset_last and $bsize == $bsize_last ) { # not tested!
     print " W: hardlink: $path$file\n";  # `rm -rf "./$dir$iso_fn$path$file"`; `ln -rs "./$dir$iso_fn$path_last$file_last" "./$dir$iso_fn$path$file"`;
     next;
    }
    
    my $tail_size = $bsize*2048 - $size ;
    my $bskip = ($offset + $bsize)*2048 - $tail_size ;
    my $a = `dd if="$iso" bs=2048  count=$tail_size skip=$bskip status=none iflag=skip_bytes,count_bytes `;
    my $b = `dd if=/dev/zero bs=2048   count=$tail_size status=none iflag=count_bytes  `;
    if ( $a ne $b) { 
      my $skip = $offset + $bsize -1; #   print "found trailing bytes for $file ($bskip, $tail_size), adding them to .mdat \n";
      `dd if="$iso" bs=2048 count=1 skip=$skip conv=sync status=none >> "$dir/$iso_fn.mdat"`; 
       print $FILE1 ("$skip,1,fill,fill,fill\n");
    }
  
    ( $path_last, $file_last, $offset_last, $bsize_last ) = ( $path, $file, $offset, $bsize );    
  }


my $diff = $original_iso_size - ($offset_last+$bsize_last)*2048;
if ( $diff > 0 ) {
  print "tail found, adding it to mdat\n" ;
  my $skip = ($offset_last + $bsize_last)*2048;
  `dd if="$iso" bs=1 skip=$skip count=$diff >> "$dir/$iso_fn.mdat"`;
}

close($FILE1);
`fusermount -u $tmp_dir/isomnt/`; 

return;
}





sub enc 
{
my ( $offset_last, $bsize_last, $original_iso_size, $original_iso_sha256, $mdatf, $offs, $file_dat, $out, $FILE0, $FILE1 ); # offset in 2048 blocks, size in bytes.
my $dd_opt='bs=2048 conv=sync status=none'; # sync -> pad with zeroes
my $last_skip = 0;

local $/=undef;
open( $FILE0, "<:raw", "$dir/$iso_fn.mdat" ) or die "ERR: $dir/$iso_fn.mdat [$!]\n"; $mdatf = <$FILE0>;  close($FILE0);
open( $FILE0, "<", "$dir/$iso_fn.offs" ) or die "ERR: $dir/$iso_fn.offs [$!]\n"; $offs = <$FILE0>;  close($FILE0);
my @lines = sort { (split(',', $a))[0] <=> (split(',', $b))[0] } (split /\n/, $offs);
`> "$iso"`; 

my $offs2 = `find './$dir$iso_fn/'  -type f -printf '%s\t%p\n' `;
my @lines2 = sort { (split(' ', $a))[0] <=> (split(' ', $b))[0] } (split /\n/, $offs2);

foreach my $line (@lines) {
  my ( $offset, $size, $crc32, $path, $file ) = ( $line=~/^([^,]+?),(\d+?),([0-9a-fA-F]+?|fill),(.+?),([^\/]+?)$/ ); # print  "$offset,$size,$crc32,$path$file\n";
  if ( $offset == -1 ) { $original_iso_size = $size; $original_iso_sha256 = $crc32; next; }
  my $bsize = ceil ($size/2048);

  if ( ! length $offset_last  ) { `dd if='$dir/$iso_fn.mdat'    count=$offset skip=0 $dd_opt > '$iso'`;   $last_skip = $offset;   }
  elsif ( $bsize == 0  ) { next; } #empty
  elsif ( $crc32 eq 'fill' ) {
    my $seek = $offset_last + $bsize_last - 1; #  print " inserting trailing block  ( $seek ) from .mdat ( $last_skip )\n";
    `dd if='$dir/$iso_fn.mdat'  of='$iso' count=1 skip=$last_skip seek=$seek  bs=2048 status=none `;
    $last_skip = $last_skip + 1;
  }
  elsif (  $offset > $offset_last + $bsize_last  and  $bsize > 0 ) {
    my $count = $offset - ( $offset_last + $bsize_last );
    print " inserting missing blocks ($offset_last, $bsize_last) from .mdat ($last_skip, $count)\n"; 
    `dd if='$dir/$iso_fn.mdat'  count=$count skip=$last_skip  $dd_opt >> '$iso'`;
    $last_skip = $last_skip + $count;
  }
  elsif ( $offset == $offset_last  and  $bsize == $bsize_last ) { print "W: skipping hardlink!\n"; next; }
  
  my $fn = "$dir$iso_fn/$path/$file";
  $fn =~ s/'/'\\''/g;
  `dd if='$fn'  count=$bsize skip=0 $dd_opt  >> '$iso' 2>/dev/null `;
  if ( $? == 256 ) { my $fn = "$dir$iso_fn/" . lc ($path) . uc "/$file"; $fn =~ s/'/'\\''/g; `dd if='$fn'  count=$bsize skip=0 $dd_opt  >> '$iso' 2>/dev/null `; }
  if ( $? == 256 ) { my $fn = "$dir$iso_fn/" . lc $path . "/$file"; $fn =~ s/'/'\\''/g; `dd if='$fn'  count=$bsize skip=0 $dd_opt  >> '$iso' 2>/dev/null `; }
  if ( $? == 256 ) { 
    foreach my $line2 (@lines2) {
      my ( $size2, $fpath2 ) = ( $line2=~/^(\d+?)\s+(.+)$/ );
      if ( $size == $size2 ) {
        local $/; open( $FILE1, "< :raw :bytes", $fpath2 ) or die "ERR_0: $fpath2 [$!]\n";;  my $datat = <$FILE1>;  close($FILE1);
        if ( !$datat ) { print "$line2 -> $fpath2\n"; exit; }
        my $crc32t = crc32_hex "$datat";  #print "[$crc32] eq [$crc32t]\n";
        if ( $crc32 eq $crc32t ) {
           $fpath2 =~ s/'/'\\''/g;
           `dd if='$fpath2'  count=$bsize skip=0 $dd_opt  >> '$iso' 2>/dev/null `;  # print "found: $fn -> $line2\n $crc32 ? $crc32t\n";
           last;
        }
      }
    }
  }
  
  
  ($offset_last, $bsize_last) = ( $offset, $bsize  );
}


my $restored_iso_size = -s "$iso";
if ( $restored_iso_size < $original_iso_size ) { my $tail = $original_iso_size - $restored_iso_size;  print " adding tail [$tail]\n"; `tail --bytes=$tail '$dir/$iso_fn.mdat' >> '$iso'`; }
my $restored_iso_sha256 = substr (`sha256sum '$iso'`, 0, 64);

print "original: [$original_iso_sha256], restored: [$restored_iso_sha256]\n";
if ( $original_iso_sha256 eq $restored_iso_sha256 ) { print ".. identical, SHA256 OK for $iso!\n"; }
else { print "ERR: SHA256 different for $iso!\n"; }

return;
}




# IFS=$'\n'; for f in $(find ./in/* -iname "*.iso" -type f ); do time perl ~/bin/reiso/reiso.pm  dec "$f"  out/  /mnt/iso/ ; done
# IFS=$'\n'; for f in $(find ./ -iname "*.iso" -type f ); do isoinfo -d  -i "$f"   >>../isoinfo.log ; done
# IFS=$'\n'; for f in $(cd in/; ls *.iso ); do  time perl ~/bin/reiso/reiso.pm  dec "in/$f" out/  /mnt/iso/ ; done
# IFS=$'\n'; for f in $(cd in/; ls *.iso ); do  time perl ~/bin/reiso/reiso.pm  enc "$f" out/  >>enc.log ; done

