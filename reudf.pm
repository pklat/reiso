#!/usr/bin/perl
# reudf v1.4.2, by orange.   requirements: udfdump, dd
#   perl reudf.pm dec in/mydvd.iso out/ mount/
#   perl reudf.pm enc mydvd.iso out/

use POSIX;
use strict;
use warnings;
use utf8;
use Encode qw(decode encode);
use File::Basename;
use Digest::SHA qw(sha256);
use Digest::CRC qw(crc32  crc32_hex);
# use warnings FATAL => 'uninitialized';
use Data::Dumper; # print Dumper($boo);


my ( $dir, $tmp_dir, $file, $iso, $iso_fn, $mode );
my $fstart = 0;
if ( @ARGV < 3 or @ARGV > 4 ) {
  print "Usage:\nto decode:\n reudf.pm   dec inputfile  output_dir/  mountpoint_dir/\nto recode:\n reudf.pm   enc outputfile  input_dir/\n";
  print "\n\nexample:\n perl reudf.pm dec in/mycd.iso out/ mount/\n";  print " perl reudf.pm enc mycd.iso out/\n"; exit;
}
$mode=$ARGV[0]; $iso=$ARGV[1]; $dir=$ARGV[2];
$iso_fn = basename($iso);
if ( not -d $dir) { print "No such dir: $dir \nPlease create it first.\n"; exit; }


if ( $ARGV[0] eq 'dec' ) {  print "Decoding $iso..\n";  &dec_udf; }
elsif ( $ARGV[0] eq 'enc' ) {  print "Encoding $iso..\n";  &enc_udf; }
exit;



sub dec_udf
{
my $original_iso_size = -s "$iso";
my $original_iso_sha256 = substr (`sha256sum "$iso"`, 0, 64);
my ( @oall, $path, @row, $dir0, @dir0, @dir1);
my ( %dirs2, %dirs, $bsize, $offset_last, $size_last, $crc32, $offset, $TAG, $sn, $type, $filename, $sector, $size, $root );
my $status = 0; # my $flag = 0;

my $dev = `udisksctl loop-setup --no-user-interaction -f  '$iso'`;
$dev =~ s/.*?as (.dev.loop\d+)/$1/;
my $u = `printf \$USER`; print " dev=$dev\n";
`sleep 3; rsync -av /media/$u/*/*   "$dir$iso_fn" `;  `chmod -R u+w "$dir/$iso_fn" ; sync `;

my @lines = split /\n/, `udfdump -t -u3   -b 2048  '$iso' | grep -E "Partition.starts.at.sector|TAG:|blob.at.sector|Filename|characteristics|:Rootdir" `;
open( my $FILE1, ">", "$dir/$iso_fn.offs" ) or die "ERR: write file [$!]\n";
print $FILE1 "-1,0,$original_iso_size,$original_iso_sha256,$iso_fn\n";

foreach my $line (@lines) {
  if ( $line =~ /Partition starts at sector/ ) {  ( $offset ) = ( $line=~/Partition starts at sector\s*(\d+)/ ); } # Partition starts at sector 257 for 4164159 sectors
  elsif  ( $line =~ /TAG: descriptor/ ) {  ( $TAG, $sn ) = ( $line=~/TAG: descriptor\s*(\d+), serial_num.*?at sector\s*(\d+)/ ); }   #prethodni
  elsif  ( $line =~ /File characteristics/ ) {  ( $type ) = ( $line=~/File characteristics\s*(\d+)/ ); }
  elsif  ( $line =~ /Filename/ ) {  ( $filename ) = ( $line=~/Filename\s*`(.*?)`/ );  }
  elsif  ( $line =~ /blob at sector/ ) {
    next if ( $TAG != 256 and $TAG != 257 );
    ( $sector, $size ) = ( $line=~/blob at sector\s*(\d+)\s*for\s*(\d+)/ );
    $bsize = ceil ($size/2048);
    my $offset = $offset + $sector;
    if ( $TAG == 256 ){
      print " root: $offset  $line\n";
      $dirs{$sector}='root';
      if ( $size > 2048 ) { $dirs{$sector+1}='root'; }
      $root=$offset; push @dir0, [ $TAG, $offset, $size, 'root', 'root', $sector ];
      next;
    }
    my $dat =  `dd if='$iso' bs=2048 skip=$offset count=$size status=none iflag=count_bytes`;
    if ( $type==0 ) {
      $crc32 = crc32_hex( $dat );
      if ( ! -e "$dir/$iso_fn.mdat" ){ print " head [0,$offset] \n"; `dd if="$iso"  bs=2048 count=$offset conv=sync status=none >"$dir/$iso_fn.mdat"`; }
    }
    elsif ( $type == 2 ) { $crc32 = '*dir*'; }
    $dirs{$sector}="$filename";
    if ( $type == 2 and $size > 2047 ) { # large dir
      for my $i (1..$bsize){ $dirs{$sector+$i}="$filename"; $dirs2{$sector+$i}=$sn; }
    } 
    $dirs2{$sector}=$sn; # sn je prethodni
    push @dir0, [ $TAG, $offset, $size, $crc32, "$filename", $sector ] if ( $type==0 ) ;
#     print "$TAG, $offset, $size, $crc32, $filename, $sector\n" if ( $type==0 ) ; #
#     print "$filename sector=$sector sn=$sn size=$size \n" if ( $type==2 ) ;
  }
}

  @dir1 = sort { $a->[1] <=> $b->[1] } @dir0;
  foreach my $row ( @dir1 ) {
    my $parent = '';
    my ( $TAG, $offset, $size, $crc32, $filename, $sector ) = ( $row->[0], $row->[1], $row->[2], $row->[3], $row->[4], $row->[5] );
    next if ( $TAG ==256 or $crc32 eq '*dir*' ); #samo fajlove  # print " $TAG,$offset,$size,$crc32,$filename,$sector\n"; #exit;
    my $path="";
    while ( $parent ne 'root' ) {
       $parent = $dirs{$dirs2{$sector}}; if (! $parent ) { print "err0: file=$filename sec=$sector \n"; exit; };
       $path = "$parent\/$path" if ( $parent ne 'root' );
       $sector = $dirs2{$sector}; #print " parent=$parent  newsector=$sector \n"; #prethodni  
       next;
    }
    if ( ! -e "$dir/$iso_fn/$path$filename" ) { print "NOT FOUND: $dir/$iso_fn/$path$filename \n"; exit; }
   $parent = '';
   $row->[4] = "$path$filename";
   print $FILE1 join(",", @{$row}), "\n";
  }
  
  close($FILE1);
  `udisksctl unmount --no-user-interaction -b $dev`;

  my ( $last_offset, $last_len ) = ( $dir1[-1][1], $dir1[-1][2] );
  my $delta = $original_iso_size - ($last_offset*2048+ceil ($last_len/2048) * 2048) ;
  if ( $delta > 0 ) { print " tail found [$delta]\n"; `tail  --bytes=$delta  "$iso" >> "$dir/$iso_fn.mdat"`; }
  return;
}





sub enc_udf
{
  my ( $mdatf, $offs, $file_dat, $out, $FILE0, $FILE1 );
  local $/=undef;
  open( $FILE0, "<:raw", "$dir/$iso_fn.mdat" ) or die "ERR: $dir/$iso_fn.mdat [$!]\n"; $mdatf = <$FILE0>;  close($FILE0);
  open( $FILE0, "<", "$dir/$iso_fn.offs" ) or die "ERR: $dir/$iso_fn.offs [$!]\n"; $offs = <$FILE0>;  close($FILE0);
  my @lines = sort { (split(',', $a))[1] <=> (split(',', $b))[1] } (split /\n/, $offs);
  my ( $original_fn, $original_iso_sha256, $original_iso_size, $offset_last, $bsize_last, $file_last );
  my $last_skip = 0; # za missing
  my $cnt = 0; # za 1Gb

  foreach my $line (@lines) {
    if ( substr ( $line, 0, 1 ) eq '-' ) { ( $original_iso_size, $original_iso_sha256, $original_fn ) = ( $line=~/^-1,0,(\d+?),(.+?),(.+?)$/ ); next; }
    my( $TAG, $offset, $size, $crc32, $file, $sn ) = ( $line=~/^([0-9\-].+?),([0-9\-].+?),(\d+?),(.+?),(.+?),(\d+)$/ );
    if ( $crc32 =~ /dir/ ) { $last_skip = $offset; next; }
    open( $FILE0, "<:raw", "$dir/$iso_fn/$file" ) or die "ERR_0: [$dir$iso_fn/$file] [$!]\n"; $file_dat = <$FILE0>;  close($FILE0);
    my $bsize = ceil ($size/2048);
    if ( ! length $offset_last  ) {
        print "head [$offset]\n";
        `dd if="$dir/$iso_fn.mdat"  count=$offset skip=0 bs=2048  status=none > "$iso"`; #head
        `dd if="$dir/$iso_fn/$file"  count=$bsize skip=0  bs=2048  status=none conv=sync  >> "$iso"  `; # prvi
        $last_skip = $offset;
    }
    elsif ( $file_last and $file eq $file_last  ) { # 1Gb split
     $cnt = $cnt + $bsize_last*2048;
     print "1gb, cnt=$cnt,   count=$bsize \n"; #exit;
     `dd if="$dir/$iso_fn/$file" count=$bsize skip=$cnt seek=$offset  bs=2048 status=none conv=sync  iflag=skip_bytes >> "$iso" `; #  count_bytes skip_bytes oflag=seek_bytes of="$iso" 
    }
    else { `dd if="$dir/$iso_fn/$file"  count=$bsize skip=0 seek=$offset  bs=2048 status=none conv=sync  >> "$iso"`; $cnt=0; } #norm
    if ( $bsize_last and $offset != $offset_last + $bsize_last ) { # missing
     print "ERR inserting missing block ($offset_last, $bsize_last) from .mdat!\n"; exit; # not yet
#      my $count = $offset - ( $offset_last + $bsize_last );
#      `dd if="$dir/$iso_fn.mdat"  count=$count skip=$last_skip  bs=2048  status=none >> "$iso"`;
#      $last_skip = $last_skip + $count;
    }
    ( $offset_last, $bsize_last, $file_last ) = ( $offset, $bsize, $file );
 }
 
  my $restored_iso_size = -s "$iso";
  if ( $restored_iso_size < $original_iso_size ) {
    my $tail = $original_iso_size - $restored_iso_size;
    print "\n $original_iso_size - $restored_iso_size   -> adding tail, size = $tail \n\n";
    `tail --bytes=$tail "$dir/$iso_fn.mdat" >> "$iso"`;
  }

my $restored_iso_sha256 = substr (`sha256sum "$iso"`, 0, 64);
print "original_iso_sha256: [$original_iso_sha256]\nrestored_iso_sha256: [$restored_iso_sha256]\n";
if ( $original_iso_sha256 eq $restored_iso_sha256 ) { print ".. identical, SHA256 OK!\n"; }
else { print "ERR: SHA256 different from original .iso!\n"; }

return;
}
 
