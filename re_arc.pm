#!/usr/bin/perl
# re_arc v1.3

use 5.012;
use strict;
use autodie;
use utf8;
use warnings FATAL => 'uninitialized';
no warnings 'portable';    # Support for 64-bit ints required

use File::Basename;
use File::Path qw/make_path/;
use Data::Dumper;    # print Dumper($boo);



if ( not $ARGV[1] ) { print "usage:\n re_arc.pm -unpack  file.psarc\n re_arc.pm -pack  file.psarc.toc\n re_arc.pm -fix  file.psarc\n"; exit; }
my ( $data, @manifest );
my $map = `dd if="$ARGV[1]" count=32 2>/dev/null`;


my ( $magic, $ver1, $ver2, $cmp_type, $toc_len, $toc_entry_size, $toc_entries, $block_size, $archive_flags ) = unpack 'a4 n n a4 N N N N N', $map;
if ( $magic ne 'PSAR' or $cmp_type ne 'zlib' ) { print "error, not zlib PSARc!\n"; exit; }
print " $ARGV[1]: $magic\_v$ver1.$ver2 $cmp_type, toclen=$toc_len, entries:$toc_entries, bs=$block_size, flag=$archive_flags\n" if ( $ARGV[0] eq '-unpack' );

my $len1 = $toc_entries * $toc_entry_size;
my $len2 = $toc_len - 32 - $len1;
$map = `dd iflag=count_bytes if="$ARGV[1]" count=$toc_len bs=4K 2>/dev/null`;
my ( $t, $TOC, $block_length_table ) = unpack "a32 a$len1 a$len2", $map;
my (@entries) = ( $TOC =~ /(.{$toc_entry_size})/gs );
my (@blocks)  = ( $block_length_table =~ /(..)/gs );
my $unp = '';
my $f = $ARGV[1];



# manifest unpack
my ( $md5, $idx, $len, $ofst0 ) = unpack ( 'H32 N H10 H10', $entries[0] );
( $len, $ofst0 )   = ( hex $len , hex $ofst0 ) ;
my $len_unp = $len ;
# print "id=$idx, len=$len, o=$ofst0, $md5\n"; #if ( $idx> 4 ) { last;}
while ( $len > 0 ) {
  my $len_cmp = unpack "n", $blocks[$idx];
#   print " id=$idx, len_cmp=$len_cmp, o=$ofst0, \n"; #if ( $idx> 4 ) { last;}
  if ( $len_cmp > 0 and $len_cmp != $len ) { # ?~ /^\x78\xDA/
     $unp = $unp.`dd if="$ARGV[1]" conv=notrunc iflag=skip_bytes skip=$ofst0 bs=$block_size count=$len_cmp 2>/dev/null | pigz -d --zlib  2>/dev/null`;
  }
  else {
     if ( $len_cmp==0 ) { $len_cmp = $block_size; }   # valjda
     $unp = $unp . `dd if="$ARGV[1]" iflag=skip_bytes,count_bytes skip=$ofst0 bs=$block_size count=$len_cmp 2>/dev/null `;
  }
  $toc_len = $toc_len + $len_cmp;
  $ofst0 = $ofst0 + $len_cmp;
  $len = $len - $block_size;
  $idx++;
}
if ( $len_unp != length ( $unp ) ) { print "ERR-manifest-len $len_unp != " . length ( $unp ) ."\n"; exit; }
$unp =~ s/\x00+$//; # sa kraja
@manifest = split( /\n/, $unp );
if ( $manifest[0] !~ /^\// ) { @manifest = map "/$manifest[$_]", 0..$#manifest; }
# print Dumper(@manifest); exit;



if    ( $ARGV[0] eq '-unpack' ) { &unarc; }
elsif ( $ARGV[0] eq '-pack' )   { &rearc; }
elsif ( $ARGV[0] eq '-fix' )   { &fix; }

exit;






sub unarc {
  my $psarc_len = -s "$f";
  my ( $len_cmp, $ofst, $md5, $idx, $len );
  for my $j ( 1 .. $#entries ) {
#     next if ( $j < 40 ); last if ( $j > 50 );
    make_path( 'out/' . "$f" . dirname( $manifest[$j-1] ) );
    open( FILE2, ">", "out/$f$manifest[$j-1]" ) or die "ERR out/$f$manifest[$j-1]  [$!]\n";
    FILE2->autoflush(1);
  
    $unp  = '';
    ( $md5, $idx, $len, $ofst ) = unpack 'H32 N H10 H10', $entries[$j];
    ( $len, $ofst )  = ( hex $len, hex $ofst );
    my $len_unp = $len;
    $len_cmp = unpack "n", $blocks[$idx];
#     print "id=$idx, len=$len, len_cmp=$len_cmp, o=$ofst, $md5  $manifest[$j-1]\n"; #if ( $idx> 4 ) { last;}
    
    while ( $len > 0 ) {
      $len_cmp = unpack "n", $blocks[$idx];
      if ( $len_cmp > 0 and $len_cmp != $len ) { # ?~ /^\x78\xDA/
#           `dd if="$ARGV[1]" of=packed/$idx.pak iflag=skip_bytes,count_bytes skip=$ofst bs=$block_size count=$len_cmp 2>/dev/null `;
          $unp = $unp . `dd if="$ARGV[1]" iflag=skip_bytes,count_bytes skip=$ofst bs=$block_size count=$len_cmp 2>/dev/null | pigz -d --zlib  2>/dev/null`;
      }
      elsif ( $len_cmp == 0 or $len_cmp == $len ) { 
          $len_cmp = $block_size  if ( $len_cmp == 0) ;
          $unp = $unp . `dd if="$ARGV[1]" iflag=skip_bytes,count_bytes skip=$ofst bs=$block_size count=$len_cmp 2>/dev/null `;
      }
      else { 
         print "error9\n"; exit; 
         }
        $ofst = $ofst + $len_cmp;
        $len = $len - $block_size;
        $idx++;
    }
    if ( $len_unp != length ( $unp ) ) { print "ERR-len $len_unp != " . length ( $unp ) ."\n"; exit; }
    print FILE2 $unp;
    close FILE2;
  }
#   print "psarc len=$psarc_len,   ofset= $ofst   len_cmp= $len_cmp\n\n"; exit;
  if ( $psarc_len > $ofst ) {
    print " W: psarc_len=$psarc_len  >  ofset=$ofst,  saving trailing garbage to $f.toc.t \n\n";
    `dd if="$f" of="$f.toc.t" iflag=skip_bytes bs=4K skip=$ofst 2>/dev/null`;
  }
  `dd if="$f" of="$f.toc" iflag=count_bytes bs=4K count=$toc_len 2>/dev/null`;
  return;
}




# dd.... | perl -0777 -pe 'for $o (1, 4) {substr($_, $o, 1) = "|"}'
# | perl -0777 -pe 'substr($_, 0, 2) = "de"'

sub rearc {
  my $toc;
  $f =~ s/.toc$//; #    print ">$f<";  exit;
  my $out = "$f.out";
  my $result;
  my $lvl=9;
  my $fix='';
  `rm -f "$out"; cp "$ARGV[1]" "$out"`; #exit;
  { local $/; open( my $fh1, "< :raw :bytes", "$ARGV[1]" ); $toc = $fh1; close $fh1; }


  for my $j ( 1 .. $#entries ) {
#     last if ( $j > 2 );
    my ( $md5, $idx, $len, $ofst ) = unpack 'H32 N H10 H10', $entries[$j];
    ( $len, $ofst )  = ( hex $len, hex $ofst );
    my $len_cmp = unpack "n", $blocks[$idx];
#     print "$j id=$idx, len=$len, len_cmp=$len_cmp, o=$ofst, $md5  $manifest[$j-1]\n";
    my $ofst2 = 0;
    while ( $len > 0 ) {
#       print " id=$idx, len=$len, len_cmp=$len_cmp, o=$ofst, o2=$ofst2\n";
      $len_cmp = unpack "n", $blocks[$idx];
      if ( $lvl == 8 ) { $fix = "perl -0777 -pe 'substr(\$_, 0, 2) = \"\\x78\\xDA\"' |"; } #header fix
      else { $fix = ''; }
      if (  $len_cmp > 0 and $len_cmp != $len ) {
#         `dd if="out/$f$manifest[$j-1]" iflag=skip_bytes skip=$ofst2 bs=$block_size count=1 2>/dev/null | pigz -$lvl --zlib --no-time --no-name >packed/$idx.rep`;
        $result = `dd if="out/$f$manifest[$j-1]" iflag=skip_bytes skip=$ofst2 bs=$block_size count=1 2>/dev/null | pigz -$lvl --zlib --no-time --no-name | $fix dd of="$out" conv=notrunc oflag=seek_bytes  seek=$ofst bs=$block_size 2>&1`;
        $result =~ s/.*?(\d+)\s+bytes.*/$1/s; #         20461 bytes (20 kB, 20 KiB) copied, 0.0220031 s, 930 kB/s
        if ( $result != $len_cmp ) {
          print " ERROR_len_cmp  result != len_cmp ( $result != $len_cmp ) level=$lvl RETRYING!\n"; #exit;
          if ( $lvl > 0 ) { $lvl -= 1; print "lvl=$lvl\n"; } # trying to fix level
          else { print " ERROR_len_cmp2  result != len_cmp ( $result != $len_cmp ) EXIT!\n"; exit; }
          next;
        }
        $ofst = $ofst + $len_cmp;
      }
      else {
        `dd if="out/$f$manifest[$j-1]" of="$out" iflag=skip_bytes conv=notrunc oflag=seek_bytes  skip=$ofst2 seek=$ofst bs=$block_size count=1  2>&1 1>/dev/null`;
        $ofst = $ofst + $block_size;
      }
      ( $len, $ofst2 ) = ( $len - $block_size, $ofst2 + $block_size  );
      $idx++;
    }
#     }
#     else {  print "Else exit..\n\n"; exit;  }
  }
  if (-e "$f.toc.t" ) { print " $f.toc.t found! fixing..\n"; &fix; }
  return;
}


#  fix for .psarc with trailing garbage
sub fix {
  my $ofst = -s "$f.out";
  if ( not -e "$f.toc.t" ) { print " file $f.toc.t not found!\n"; return; }
  `dd if="$f.toc.t" of="$f.out" conv=notrunc oflag=seek_bytes  seek=$ofst bs=4K  2>&1 1>/dev/null`;
  return;
}



