#!/usr/bin/perl
# reorder v0.95

use 5.012;
use strict;
use warnings;

use utf8;
use File::Find::Rule;
use List::Util qw(first);
use IO::Handle;
use Data::Dumper; # print Dumper($boo);
use warnings FATAL => 'uninitialized';

if (not $ARGV[1] ) {print "usage:\n reorder.pm inputdir  outdir \n"; exit;}

my ( $level, $packed, $dir_in, $dir_out, $size_raw, $size_packed, $delta, $delta_max, $delta_max2, $best_match, $best_match2, $total, $totaldmax, %datatype, %size );
my ( $ramdisk, $window_i, $window_j, $half, $cmd, $sub, $index, @ssdeep, %leaders, %followers, %cluster );
my %done; # processed files

my %diff = ( lz4 => \&diff_lz4,
             zstd => \&diff_zstd,
             sevenzip => \&diff_7z 
           );
$sub = 'lz4';
$level = 2;
$ramdisk = '/mnt/ramdisk'; # RAMDISK
$packed = "$ramdisk/packed_qq.lzx"; # temp
$total = $totaldmax = 0;
$dir_in = $ARGV[0];
$dir_out = $ARGV[1];
$window_i = 1024*1024; # first file, bytes
$window_j = 1024*1024; # second file
$index = 0;

open( FILE0, ">", "$dir_out/sorted.log" ) or die "Can't write file [$!]\n"; FILE0->autoflush(1);
open( FILE1, ">", "$dir_out/sorted.stats" ) or die "Can't write file [$!]\n"; FILE1->autoflush(1);


my @symlinks = File::Find::Rule->symlink()->in("$dir_in");
my @files = File::Find::Rule->file()->not(File::Find::Rule->symlink())->in("$dir_in");
if ( !$files[0] ) { print "no files found! \n"; exit; }
@files = sort @files;


# ssdeep   -> real    +0m47.676s
if ( not -e "$dir_out/hash.ssdeep" or not -e "$dir_out/hash_grouped.ssdeep" ) { `ssdeep -r $dir_in > $dir_out/hash.ssdeep`; `ssdeep -xg  $dir_out/hash.ssdeep >$dir_out/hash_grouped.ssdeep`;}
my @groups = split ('\n', `cat $dir_out/hash_grouped.ssdeep` );
# exit;

# datatypes  -> real    +0m14.345s
for (my $i = 0; $i <= $#files; $i++ ) {
#  print "$files[$i]\n";  print `file -b $files[$i] | awk -F',' '{print \$1}'` . "\n";
 $datatype{$i} = `file -b $files[$i] |awk -F',' '{print \$1}'`; chomp $datatype{$i};
 $size{$i} = -s $files[$i];
}

unlink $packed;
my $cluster_size=0;
my $leader = -1;
my $flag = 0;


# read groups into ssdeep
for my $g (0 .. $#groups)  #   ssdeep [] = $i    $leaders{$i} = cluster_size   followers = hash , key=leader   cluster {leader} = grupa_bez_leadera[]
{
  if ( $groups[$g] =~ m/^\*\* Cluster size (\d+)/ ) { $flag = 1; next; }
  elsif ( $groups[$g] !~ m/\w+/ ) { next; } # empty line
  
  ++$index until $files[$index] eq $groups[$g] or $index > $#files-1; # nadji $i
  if ($index > $#files-1) { $index = 0; next; }; #nije nadjen (symlink)
  if ( $flag eq 1 ) { $leader = $index; } # print " leader: $index -> $files[$index]\n"; 
  else { $followers{$index} = $leader ;  push @{$cluster{$leader}}, $index; $done{$index} = 1; $leaders{$leader} = 1; }
  $index = $flag = 0;
}
# print Dumper (%leaders); exit;
# foreach my $key (keys %leaders) { print "$files[$key]\n"; }; exit;
# print Dumper (\%cluster); exit;
# print Dumper (@ssdeep); exit;


# main   ssdeep 
my $done_total = 0;
my $todo_total = $#files;
my $i = 0;
$index = 0;
print "done_total: $done_total/$todo_total\n"; #exit;


while ( $done_total < $todo_total )
{
  print "i= $i ( $files[$i] )\n"; # ** Cluster size 2
  if ( exists ($followers{$i}) ) { $i = $followers{$i}; } # found FOLLOWER goto ->leader 
  if ( exists($leaders{$i}) ) { #print cluster
    print FILE0 "$files[$i]\n"; $done_total++; #leader
    print FILE1 "ssdeep_l $files[$i]\n";
    $done{$i} = 1 if ( $i == 0 ); # first leader
    for my $c (0 .. $#{$cluster{$i}}-1) #loop cluster elements, skip last!
    {
#       print "ssdeep: leader $i=$files[$i],     $cluster_element -> $files[$cluster_element] \n";
      print FILE0 "$files[${$cluster{$i}}[$c]]\n"; $done_total++;
      print FILE1 "ssdeep $c: $files[${$cluster{$i}}[$c]]\n";
    }
    print "goto diffc ( $cluster{$i}[-1] -> $files[$cluster{$i}[-1]] ) \n";
    $i = &diffc($cluster{$i}[-1]);
    last if ( not length $i );
    print "back from diffc : $i -> $files[$i] \n";
  }
  
  else { $i = &diffc($i);    print "back from diffc0 : $i -> $files[$i] \n"; }
  print "done_total: $done_total \n\n";
}



print FILE0 join("\n", @symlinks); 


print FILE1 "-\n symlinks:\n" . join("\n", @symlinks);
print FILE1 "--\n TOTAL D=$totaldmax\n";

close(FILE0); close(FILE1);

exit; 
















sub diffc{
my ( $i ) = @_;
my $getback = 0;
# print "diffc, I=$i, $files[$i]\n"; #exit;

while ( ! $getback ) {
 $best_match = $best_match2 = $delta_max = $delta_max2 = undef;
 my $size_i = $size{$i};
 my $datatype_i = $datatype{$i};
 
 `tail --bytes $window_i $files[$i] >$ramdisk/file_i.dat`;
 
 for (my $j = 0; $j <= $#files ; $j++) {
  if ( $j==$i or exists($done{$j}) ) { next; print FILE0 "skip i=$i j=$j \n"; next;} ; # preskace sebe ili nadjene
  next if ( length $best_match  and $datatype_i ne $datatype{$j} ); # preskace datatype   
  
  $size_raw = $size_i + $size{$j}; # $options = $options . " -md=$size_raw" . 'b';
#   &window ( $i, $j );
  `tail --bytes $window_j $files[$j] >$ramdisk/file_j.dat`;
  $cmd = "cat $ramdisk/file_i.dat $ramdisk/file_j.dat  |";
  $size_packed = $diff{$sub}->( $i, $j );  # diff subroutine
  unlink $packed, "$ramdisk/file_j.dat";
  $delta = $size_raw - $size_packed; #usteda
  if ( not length $delta_max or $delta > $delta_max ) {
   if ( length $best_match ) { $best_match2 = $best_match; $delta_max2 = $delta_max; };
   $delta_max = $delta;
   $best_match = $j;
  }
#   print "S=$size_packed, $files[$i] -> $files[$j]   $best_match\n"; #exit;
 }

 unlink "$ramdisk/file_i.dat";
 if ( not length $best_match ) { print FILE0 "$files[$i]\n"; print FILE1 "$i: $files[$i]\n"; $done_total++; return undef;} # nema

  print " Dmax=$delta_max, $files[$i] -> $files[$best_match]\n"; #exit;
  print FILE0 "$files[$i]\n";   $done_total++;
  print FILE1 "$i: $files[$i] -> $files[$best_match]\n";
  $done{$i} = 1;
  $done{$best_match} = 1;
  $totaldmax = $totaldmax + $delta_max;
  if ( length $best_match2 ) {
   if ( exists($leaders{$best_match}) ) { $getback = 1; $i = $best_match; last;}
   $totaldmax = $totaldmax + $delta_max2;
   $done{$best_match2} = 1;
   print FILE0 "$files[$best_match]\n";   $done_total++;
   print FILE1 "b2 $i: $files[$i] -> $files[$best_match2]\n";
   $i = $best_match2;
  }
  else { $i = $best_match; }
  
  $getback = 1 if ( exists($leaders{$i}) ) ;
}

return $i;
} 











sub diff_7z
{
 my ( $i, $j ) = @_;
 return `7z a -myx=9 -m0=LZMA -mx=$level -ms=on  $packed \"$files[$i]\" \"$files[$j]\"  | grep "Archive size:" | awk -F' ' '{print \$3}' `;
}


sub diff_zstd
{
 my ( $i, $j ) = @_;
 my $size;
 
 $cmd = $cmd . " zstd -$level -o /dev/null 2>&1 ";
 $size = `bash -c "$cmd"`;         # /*stdin*\            : 55.16%   (1048576 => 578410 bytes, /dev/null) 
 $size =~ m/ (\d+) bytes/;
 return $1;
}



sub diff_lz4
{
 my ( $i, $j ) = @_;
 my $size;
 
 $cmd = $cmd . " lz4 - $packed 2>&1 "; #print "$cmd\n"; #exit;  #    Compressed 1048576 bytes into 725046 bytes ==> 69.15% 
 $size = `bash -c "$cmd"`;
 $size =~ m/into (\d+) bytes/;
 return $1;
}


